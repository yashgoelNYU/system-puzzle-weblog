import datetime
import os
import psycopg2

from flask import Flask, render_template
from config import configure_app

app = Flask(__name__)
PORT = os.getenv('PORT', '5000')

# set the configurations
configure_app(app)

@app.route("/", methods=('GET', 'POST'))
def index():
    # Connect to database
    conn = psycopg2.connect(host='db', database=os.environ['POSTGRES_DB'], user=os.environ['POSTGRES_USER'], password=os.environ['POSTGRES_PASSWORD'])
    cur = conn.cursor()

    # Get number of all LOCAL GET requests
    sql_all_local = """SELECT COUNT(*) FROM weblogs WHERE source = 'local';"""
    cur.execute(sql_all_local)
    all_local = cur.fetchone()[0]

    # Get number of all REMOTE GET requests
    sql_all_remote = """SELECT COUNT(*) FROM weblogs WHERE source = 'remote';"""
    cur.execute(sql_all_remote)
    all_remote = cur.fetchone()[0]

    # Get number of all succesful LOCAL requests
    sql_success_local = """SELECT COUNT(*) FROM weblogs WHERE status LIKE \'2__\' AND source = 'local';"""
    cur.execute(sql_success_local)
    success_local = cur.fetchone()[0]

    # Get number of all succesful REMOTE requests
    sql_success_remote = """SELECT COUNT(*) FROM weblogs WHERE status LIKE \'2__\' AND source = 'remote';"""
    cur.execute(sql_success_remote)
    success_remote = cur.fetchone()[0]

    # Determine rate for LOCAL requests if there was at least one LOCAL request
    rate_local = "No entries yet!"
    if success_local != 0:
        rate_local = str(success_local / all_local)

    # Determine rate for REMOTE requests if there was at least one REMOTE request
    rate_remote = "No entries yet!"
    if success_remote != 0:
        rate_remote = str(success_remote / all_remote)

    return render_template('index.html', rate_local = rate_local, rate_remote = rate_remote)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=int(PORT))
