import os
basedir = os.path.abspath(os.path.dirname(__file__))

class BaseConfig(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = 'super_duper_secret'

class ProductionConfig(BaseConfig):
    DEBUG = False
    ENV = 'prod'
    SECRET_KEY = 'super_duper_secret'

class DevelopmentConfig(BaseConfig):
    DEVELOPMENT = True
    ENV = 'dev'
    DEBUG = True
    SECRET_KEY = 'super_duper_secret'

class TestingConfig(BaseConfig):
    TESTING = True
    ENV = 'test'
    SECRET_KEY = 'super_duper_secret'

config = {
    "dev": "config.DevelopmentConfig",
    "prod": "config.ProductionConfig",
    "test": "config.TestingConfig",
    "default": "config.DevelopmentConfig"
}

def configure_app(app):
    config_name = os.getenv('FLASK_CONFIGURATION', 'default')
    app.config.from_object(config[config_name])
